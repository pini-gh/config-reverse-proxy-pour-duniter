[[_TOC_]]

# Pourquoi utiliser un mandataire inverse (reverse proxy)

Les avantages liés à l'utilisation d'un mandataire inverse sont multiples :
* Partage des ports HTTP / HTTPS standards entre différents services
* Prise en charge de la [terminaison SSL](https://en.wikipedia.org/wiki/TLS_termination_proxy) pour des services ne le gérant pas nativement
* Possibilité d'héberger plusieurs services sur la même machine
* ...

La mise en place est relativement simple, pourvu qu'on prenne le temps de se documenter un peu (et ce ne sera pas du temps perdu). Cette page ne traite pas de la configuration générale d'un mandataire inverse. Il existe déjà de nombreuses documentations sur le sujet :
* [Configuration pas à pas d'un reverse proxy nginx](https://openclassrooms.com/fr/courses/1733551-gerez-votre-serveur-linux-et-ses-services/5236081-mettez-en-place-un-reverse-proxy-avec-nginx)
* [Apache - Guide de configuration d'un mandataire inverse](https://httpd.apache.org/docs/2.4/fr/howto/reverse_proxy.html)

Dans les chapitres suivants nous allons présenter quelques exemples de configuration. Il faut bien avoir à l'esprit qu'il s'agit d'exemples, et qu'il n'y a pas une solution unique. N'hésitez pas à vous documenter et à croiser les informations pour améliorer votre compréhension et mettre en place la configuration qui vous conviendra le mieux.

# Générer ses certificats SSL
## Principe

> Un certificat électronique (aussi appelé certificat numérique ou certificat de clé publique) peut être vu comme une carte d'identité numérique. Il est utilisé principalement pour identifier et authentifier une personne physique ou morale, mais aussi pour chiffrer des échanges.
>
> *(...)*
>
> Il est signé par un tiers de confiance qui atteste du lien entre l'identité physique et l'entité numérique (virtuelle).

*source [wikipédia](https://fr.wikipedia.org/wiki/Certificat_%C3%A9lectronique)*

Si initialement le protocole HTTPS était plutôt réservé aux données sensibles, il est devenu la norme car il empêche tout intermédiaire réseau d'interférer avec vos données. La plupart des navigateurs internet affichent maintenant une alerte lorsque HTTPS ne peut pas être utilisé.

## LetsEncrypt

[LetsEncrypt](https://letsencrypt.org/) est une autorité de certification à but non lucratif et permet de générer gratuitement ses certificats SSL. Il joue le rôle du tiers de confiance expliqué ci-dessus.

Si vous avez déjà vos certificats, vous pouvez passer cette étape. Assurez-vous juste de les renseigner correctement dans la configuration de Nginx ou Apache.

Pour générer un certificat nous utilisons ici l'utilitaire [certbot](https://certbot.eff.org/) disponible dans la plupart des distributions Linux. Nous montrons ci-dessous un exemple de génération de certificats, mais vous êtes vivement invités à vous documenter sur certbot ou d'[autres outils de la galaxie Let's Encrypt](https://letsencrypt.org/docs/client-options/), et à configurer l'automatisation de la génération et du renouvellement du certificat.

Si vous utilisez Docker, [nginx-proxy](https://github.com/nginx-proxy/nginx-proxy) et [acme-companion](https://github.com/nginx-proxy/acme-companion) prennent tous ces aspects en charge très simplement.

### Génération de certificat

Dans cet exemple nous utilisons la méthode par validation DNS.

**Pour un certificat concernant juste un sous domaine :**
```bash
certbot certonly --manual --preferred-challenges dns -d duniter.example.org
```
**Pour un certificat wildcard :**

Permet d'utiliser le même certificat pour tous les sous-domaines associés au même domaine.
```bash
certbot certonly --manual --preferred-challenges dns -d *.example.org -d example.org
```
Renseignez les différentes informations demandées et vous devriez avoir un message comme celui-ci :

```
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please deploy a DNS TXT record under the name
_acme-challenge.duniter.example.org with the following value:

T8zuEq7iBMm5_zrPVBY5sc0sZ4yiLWvYJsCh_pnMCEU

Before continuing, verify the record is deployed.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Vous devez rajouter l'enregistrement TXT spécifié dans la configuration de votre domaine, une fois fait, continuez.

À l'issue de l'opération vos certificats sont générés et vous pouvez supprimer l'enregistrement DNS TXT créé précédemment pour la vérification du domaine par Let's Encrypt.

**Attention** : les certificats de LetsEncrypt ne sont valables que trois mois. Il faut les renouveler avant leur date d'expiration (voir ci dessus les liens vers les outils permettant l'automatisation de cette tâche).
Il pourra être nécessaire de conserver l'enregistrement DNS précédemment créé.

# Configuration de Duniter

Nous utilisons ici les ports par défaut du service Duniter :
* BMA sur port 10901
* WS2P sur port 20901
* GVA sur port 30901 (Duniter version 1.9.0 minimum)
* Interface web sur port 9220

Nous supposons également que Duniter écoute sur l'interface locale (`127.0.0.1`). Dans le cas d'une installation conteneurisée, ou si le mandataire inverse n'est pas sur la même machine que le serveur Duniter, il faudra adapter l'adresse IP en conséquence (IP de l'instance Docker ou de la machine hébergeant Duniter).

Notre sous-domaine sera nommé `duniter.example.org.

L'utilisation d'un unique sous-domaine impose l'organisation suivante :
* Accès au service BMA par défaut
* Accès au service WS2P par un chemin dédié ajouté à l'URL. Nous utiliserons le chemin `ws2p`
`$ duniter config --ws2p-remote-path ws2p`
* Accès au service GVA par un chemin dédié ajouté à l'URL. Aucune congiguration n'est nécessaire car les chemins `gva` et `gva-sub` sont configurés par défaut.
* Rien n'est prévu pour l'interface web, mais nous pourrons utiliser les fonctions de ré-écriture d'URL du reverse proxy pour imposer l'utilisation d'un autre chemin, que nous nommerons `admin`.

# Configuration du mandataire inverse (reverse proxy)

## Avec nginx

Il faut définir un bloc `upstream` par service :

```
upstream duniter-bma {
    server 127.0.0.1:10901;
}
upstream duniter-ws2p {
    server 127.0.0.1:20901;
}
upstream duniter-gva {
    server 127.0.0.1:30901;
}
upstream duniter-admin {
    server 127.0.0.1:9220;
}
```

Et dans le bloc `server` associé à `duniter.example.org` pour le HTTPS, dispatcher les requêtes vers le bon backend en fonction du chemin :
```
server {
    server_name duniter.example.org;
    listen 443 ssl http2;
    ...
    # Service BMA
    location / {
        proxy_pass http://duniter-bma;
    }
    # Socket WS2P sur chemin 'ws2p'
    location /ws2p {
        proxy_http_version 1.1;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   Host            $host;
        proxy_set_header   X-Real-IP       $remote_addr;
        proxy_set_header   Upgrade         $http_upgrade;
        proxy_set_header   Connection      "upgrade";

        proxy_pass http://duniter-ws2p;
    }
    # Service GVA sur chemin 'gva'
    location /gva {
        proxy_pass http://duniter-gva;
    }
    # Socket souscription GVA sur chemin 'gva-sub'
    location /gva-sub {
        proxy_http_version 1.1;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   Host            $host;
        proxy_set_header   X-Real-IP       $remote_addr;
        proxy_set_header   Upgrade         $http_upgrade;
        proxy_set_header   Connection      "upgrade";

        proxy_pass http://duniter-gva;
    }
    # Interface d'administration via ré-écriture d'URL
    # Ne pas laisser comme ça - À protéger !
    location ~ ^/(admin|fonts?|images|webmin)/ {
        rewrite ^/admin/(.*)$ /$1 break;
        proxy_pass http://duniter-admin;
    }
}
```

**Note importante** : il faudra bien veiller à [protéger l'accès à l'interface d'aministration](#protection-de-lacc%C3%A8s-%C3%A0-linterface-dadministration).

## Avec Apache
### Activation des modules nécessaires

```bash
a2enmod ssl proxy proxy_http proxy_wstunnel rewrite headers
```

**ssl** : obligatoire pour la gestion du SSL.

**proxy** : transfert des requêtes reçu à Duniter.

**proxy_http** : proxy pour requête http.

**proxy_wstunnel** : roxy pour web socket.

**rewrite** : réécriture des urls à la volée.

**headers** : personnalisation des en-têtes des requêtes (utilisé pour la connexion aux  websockets).

### Configuration d'Apache

Les fichiers de configuration d'Apache sont généralement situés dans le répertoire `/etc/apache2` (Debian 10)

Les fichiers de configuration des différents sites sont généralement situés dans le répertoire `/etc/apache2/site-available`

Création de notre fichier de configuration : copiez le texte ci-dessous dans le fichier `/etc/apache2/sites-available/duniter.example.org.conf`

Il faudra peut-être adapter la configuration à votre OS.

```
# redirect non ssl requests to ssl port
<VirtualHost *:80>
	ServerName duniter.example.org
	<IfModule mod_rewrite.c>
		RewriteEngine on
		RewriteCond %{SERVER_PORT} !^443$
		RewriteRule ^/(.*) https://%{SERVER_NAME}/$1 [L,R]
	</IfModule>
</VirtualHost>

# ssl requests port
<VirtualHost *:443>
    ServerName duniter.example.org
    
    # activate rewrite engine
    RewriteEngine On
    # activate ssl proxy
    SSLProxyEngine on

    # proxy SSL ws2p requests to Duniter ws2p service
    RewriteCond %{HTTP:UPGRADE} ^WebSocket$ [NC,OR]
    RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
    RewriteCond %{REQUEST_URI}  ^/ws2p            [NC]
    RewriteRule .* ws://127.0.0.1:20901%{REQUEST_URI} [P,QSA,L]
	
    # Admin interface
    # to generate password: htpasswd -c /etc/apache2/duniter-admin.htpasswd <user>
    # uncomment to activate
    #RewriteCond %{HTTP:UPGRADE} ^WebSocket$  [NC,OR]
    #RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
    #RewriteRule /webmin/ws(.*) ws://127.0.0.1:9220/webmin/ws$1 [P,L]
    #RewriteRule ^/admin/(.*) http://127.0.0.1:9220/$1 [proxy,last]
    #RewriteRule ^/(webmin|images|fonts?)/(.*) http://192.168.212.35:9220/$1/$2 [proxy,last]
    #ProxyPassReverse /admin http://192.168.212.35:9220/
    #<Proxy http://127.0.0.1:9220/*>
    #    Order deny,allow
    #    Allow from all
    #    Authtype Basic
    #    Authname "Password Required"
    #    AuthUserFile /etc/apache2/duniter-admin.htpasswd
    #    Require valid-user
    #</Proxy>

    # proxy BMAS requests to Duniter BMA service
    ProxyPass / http://127.0.0.1:10901/
    ProxyPassReverse / http://127.0.0.1:10901/
    
    # proxy GVA requests to Duniter GVA service
    ProxyPass /gva http://127.0.0.1:30901
    ProxyPassReverse /gva http://127.0.0.1:30901
    ProxyPass /gva-sub http://127.0.0.1:30901
    ProxyPassReverse /gva-sub http://127.0.0.1:30901
    
    # deactivate direct proxy request 
    ProxyRequests Off
    # pass the incoming request host to the proxied host via Header
    ProxyPreservehost On
    
    RequestHeader set X-Forwarded-Proto "https"
    RequestHeader unset If-Modified-Since
    RequestHeader unset If-None-Match
    # HSTS header to inform browser to force https connexion
    Header set Strict-Transport-Security "max-age=31536000; includeSubDomains"
    
    # Activate ssl
    SSLEngine On
    
    # Strong SSL
    SSLProtocol -all +TLSv1.3 +TLSv1.2
    SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
    
    # Certificats ; example for LetsEncrypt certificats
    SSLHonorCipherOrder on
    SSLCertificateFile "/etc/letsencrypt/live/duniter.example.org/cert.pem"
    SSLCertificateKeyFile "/etc/letsencrypt/live/duniter.example.org/privkey.pem"
    SSLCertificateChainFile "/etc/letsencrypt/live/duniter.example.org/chain.pem"
</VirtualHost>
```

Notre fichier de configuration gérant le port 80, nous pouvons supprimer la configuration par défaut d'Apache et activer notre configuration.

```bash
a2dissite 000-default
a2ensite duniter.example.org
```

Rechargez la configuration d'Apache.

```bash
systemctl reload apache2
```
**Note importante** : il faudra bien veiller à [protéger l'accès à l'interface d'aministration](#protection-de-lacc%C3%A8s-%C3%A0-linterface-dadministration).

# Protection de l'accès à l'interface d'administration

Si vous ne protégez pas l'accès à l'interface d'administration n'importe qui pourra y accéder et intervenir sur votre noeud. Nous détaillons ci-dessous quelques techniques de protection.

## Accès par interface locale uniquement

Cela consiste à ne pas configurer l'accès via le mandataire inverse. Voir plus bas les directives pour nginx et Apache.

Si l'installation de Duniter est sur votre PC, vous pourrez y accéder via l'adresse locale `http://localhost:9220`, mais pas depuis l'externe.

Si l'installation est sur une autre machine que votre PC, on devra préalablement créer un [tunnel SSH](https://www.it-connect.fr/chapitres/tunneling-ssh/) :
```
$ ssh -f -N -L9220:localhost:9220 <machine_duniter>
```
L'interface devient alors accessible depuis votre PC via l'adresse locale `http://localhost:9220`.

### Configuration nginx

Supprimez ou commentez le bloc `upstream` relatif à l'interface d'administration.

Supprimez ou commentez le bloc `location` pour l'interface d'administration.

### Configuration Apache

Par défaut l'accès à l'interface d'administration via le proxy Apache est désactivé.

## Protection par mot de passe

### Configuration nginx

Voir la [documentation de nginx sur le sujet](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/).

Et ajouter en tête du bloc `location` de l'interface d'administration :

```
    auth_basic           “Interface web duniter”;
    auth_basic_user_file /chemin/fichier/motdepasse; 
```

### Configuration Apache

Pour générer le mot de passe utilisé la commande ci-dessous en tant qu'utilisateur root :
```bash
htpasswd -c /etc/apache2/duniter-admin.htpasswd < user >
```
Remplacez _< user >_ par le nom d'utilisateur voulu. Tapez ensuite votre mot de passe et validez.

Dans le fichier de configuration de votre site `/etc/apache2/sites-available/duniter.example.org.conf`, décommentez les lignes comme ci-dessous :
```
# Admin interface
# to generate password: htpasswd -c /etc/apache2/duniter-admin.htpasswd <user>
# uncomment to activate
RewriteCond %{HTTP:UPGRADE} ^WebSocket$  [NC,OR]
RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
RewriteRule /webmin/ws(.*) ws://127.0.0.1:9220/webmin/ws$1 [P,L]
RewriteRule ^/admin/(.*) http://127.0.0.1:9220/$1 [proxy,last]
RewriteRule ^/(webmin|images|fonts?)/(.*) http://192.168.212.35:9220/$1/$2 [proxy,last]
ProxyPassReverse /admin http://192.168.212.35:9220/
<Proxy http://127.0.0.1:9220/*>
    Order deny,allow
    Allow from all
    Authtype Basic
    Authname "Password Required"
    AuthUserFile /etc/apache2/duniter-admin.htpasswd
    Require valid-user
</Proxy>
```

## Protection par certificat SSL

### Configuration nginx

Voir par exemple [cette documentation](https://www.ssltrust.com.au/help/setup-guides/client-certificate-authentication) pour la génération du certificat.

Il faut ensuite ajouter les directives de vérification du certificat dans la configuration nginx.

```
server {
    ...
    ssl_client_certificate /chemin/vers/certificat.crt;
    ssl_verify_client optional;
    ...
    # Interface d'administration
    location ~ ^/(admin|fonts?|images|webmin)/ {
        if ($ssl_client_verify != SUCCESS) {
            return 403;
        }
        rewrite ^/admin/(.*)$ /$1 break;
        proxy_pass http://duniter-admin;
    }
}
```

# Tests de bon fonctionnement

### BMAS

Avec votre navigateur, connectez-vous à `https://duniter.example.org`. Vous devez arriver sur une page qui vous affiche un contenu au format JSON qui ressemble à ça :
```
"duniter": {	
  "software": "duniter",
  "version": "1.8.1",
  "forkWindowSize": 100
}
```

### WS2P

Sous Firefox, vous pouvez utiliser le plugin [Simple Websocket Client](https://addons.mozilla.org/en-US/firefox/addon/simple-websocket-client/) et tester l'URL `wss://duniter.example.org/ws2p`. Le résultat dans la zone "Message log" doit ressembler à :
```
{"auth":"CONNECT","pub":"D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx","challenge":"cac450ce-c350-492c-8711-3e8e00bae54b03f2a155-64ec-4205-a9d9-75782cdfb305","sig":"LzklUEhKA9+s8J+21BGg8b72rkJ55QwBDurlIMM0Zk0ZDLozoe26U/h/AUCbBlJ63KOIdJqUq8Z/cV/JZUw8Aw==","currency":"g1"}
```

### GVA

#### Test de la terminaison standard
Avec votre navigateur, connectez-vous à `https://duniter.example.org/gva`. Vous devez arriver sur une page permettant d'interroger le noeud.
Dans la fenêtre de gauche saisissez :
```
{
  query: currentBlock {
    number
  }
}
```
Appuyez sur le bouton _play_, vous devriez avoir en résultat (le numéro du bloc étant bien sur différent) :
```
{
  "data": {
    "query": {
      "number": 421100
    }
  }
}
```
#### Test de la terminaison _subscription_
Sous Firefox, vous pouvez utiliser le plugin [Simple Websocket Client](https://addons.mozilla.org/en-US/firefox/addon/simple-websocket-client/), connectez-vous à l'adresse `wss://duniter.example.org/vga-sub`.

Si le status de la connexion affiche "OPENED" c'est que cela fonctionne.

### Interface d'administration

Selon votre configuration et votre stratégie de protection, connectez-vous à `http://localhost:9220` ou `https://duniter.example.org/admin` et vérifiez que :
* La protection par mot de passe ou certificat SSL fonctionne (sauf connexion via `localhost`)
* L'interface s'affiche sans erreur.
